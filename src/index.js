import './scss/main.scss';

const header = document.getElementsByTagName('header')[0].offsetHeight;
const body = document.getElementsByTagName('body')[0];
const find = document.getElementById('search-button');
const searchField = document.getElementById('homepage-search');
const newsletterSubmit = document.getElementById('newsletter-top-submit');
const modalContainer = document.getElementById('modal-container');
const newsletter = document.getElementById('newsletter');
const newsletterClose = document.getElementById('close-nlmodal');
const cookiesletterClose = document.getElementById('close-ckmodal');
const thankyou = document.getElementById('thankyou');
const mainNav = document.getElementById('mainNav');
const newsletterModal = document.getElementById('newsletter-modal');
const cookiesModal = document.getElementById('cookie-modal');
const mobileMenuButton = document.getElementById('mobile-nav');
const mobileMenuCont = document.getElementById('mobile-nav-container');
let clicksCounter = 0;

// Provisional //
if(window.innerWidth > 768) {
    body.classList.remove('locked-screen');
}
// Provisional //

find.addEventListener('click', () => {
    searchField.classList.toggle('reveal-search');
});
newsletterSubmit.addEventListener('click', () => {
    event.preventDefault();
    newsletter.classList.toggle('footer-newsletter-visible');
    thankyou.classList.toggle('footer-newsletter-visible');
});
newsletterClose.addEventListener('click', () => {
    newsletterModal.classList.add('closed-modal');
    clicksCounter++;
    setTimeout(() => {
        newsletterModal.style.display = 'none';
    }, 500);
    if(clicksCounter >= 2) {
        body.classList.remove('locked-screen');
        modalContainer.style.display = "none";
    }
});
cookiesletterClose.addEventListener('click', () => {
    cookiesModal.classList.add('closed-modal');
    clicksCounter++;
    setTimeout(() => {
        cookiesModal.style.height = '0';
    }, 500);
    if(clicksCounter >= 2) {
        body.classList.remove('locked-screen');
        modalContainer.style.display = "none";
    }
});
window.addEventListener('scroll', () => {
    if((window.pageYOffset >= header) && (window.innerWidth > 768)) {
        mainNav.classList.add('locked-navigation');
    } else if ((window.pageYOffset >= header) && (window.innerWidth < 768)) {
        mobileMenuCont.classList.add('half-transparent');
    } else {
        mainNav.classList.remove('locked-navigation');
        mobileMenuCont.classList.remove('half-transparent');
    }
});
if(window.innerWidth <= 768) {
    modalContainer.style.display = 'block';
    setTimeout(() => {
        modalContainer.classList.add('opened-modal');
    }, 500);
}
mobileMenuButton.addEventListener('click', () => {
    mainNav.classList.toggle('menu-show');
    mobileMenuButton.classList.toggle('menu-show');
    body.classList.toggle('locked-screen');
});

// Start of Slider
const slider = document.getElementById('slider');
const sliderContent = document.getElementById('slider-container');
const slidesCount = sliderContent.childElementCount;
const sliderWidth = slidesCount * slider.offsetWidth;
sliderContent.style.width = sliderWidth;

if((sliderContent !== null) && (slidesCount > 1)) {
    let progress = 1;
    //for(let index of slidesCount) {
    for(let index = 0; index < slidesCount; index++) {
        document.getElementsByClassName('slider__content--slide')[index].style.width = slider.offsetWidth;
    }
    setInterval(() => {
        let leftMargin = progress * slider.offsetWidth;
        sliderContent.style.marginLeft = "-" + leftMargin + "px";
        if(progress < (slidesCount - 1)) {
            progress++;
        } else {
            progress = 0;
        }
    }, 3000);
}
// End of Slider